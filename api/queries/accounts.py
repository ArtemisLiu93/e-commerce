from pydantic import BaseModel
# from queries.pool import pool
# from typing import List, Optional


class DuplicateAccountError(ValueError):
    pass


class AccountLoggedIn(BaseModel):
    email: str
    username: str
    password: str


class AccountLoggedOut(BaseModel):
    id: int
    email: str
    username: str


class AccountOutWithPassword(AccountLoggedOut):
    hashed_password: str


class UserInfo(BaseModel):
    profile_pic: str
    interests: str
