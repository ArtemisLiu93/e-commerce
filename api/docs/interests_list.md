## SQL STATEMENTS TO GET ACTIVITIES IN THE DATABASE

# step 1: go to postgres terminal and type this: psql -h localhost -U artemis ecommerce_db

# step 2:
-- Insert activities 1-16
INSERT INTO activities (name)
VALUES
('Baseball'),
('Basketball'),
('Cross-Training'),
('Dance'),
('Football'),
('Golf'),
('Lacrosse'),
('Lifestyle'),
('Running'),
('Skate'),
('Soccer'),
('Softball'),
('Swimming'),
('Tennis'),
('Trail Running'),
('Training');

-- Insert activities 17-21
INSERT INTO activities (name)
VALUES
('Spring'),
('Summer'),
('Fall'),
('Winter');

-- Insert activities 22-27
INSERT INTO activities (name)
VALUES
('Korean Style'),
('Street Wear'),
('Retro'),
('Vintage'),
('Chic');